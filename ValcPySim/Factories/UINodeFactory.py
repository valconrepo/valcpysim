from PyFlow.Packages.ValcPySim.Nodes.EnergyBalance import EnergyBalance
from PyFlow.Packages.ValcPySim.UI.UIEnergyBalance import UIEnergyBalance
from PyFlow.Packages.ValcPySim.Nodes.ClosedTank import ClosedTank
from PyFlow.Packages.ValcPySim.UI.UIClosedTank import UIClosedTank
from PyFlow.Packages.ValcPySim.Nodes.PressureBuildingDevice import PressureBuildingDevice
from PyFlow.Packages.ValcPySim.UI.UIPressureBuildingDevice  import UIPressureBuildingDevice

from PyFlow.UI.Canvas.UINodeBase import UINodeBase


def createUINode(raw_instance):
    if isinstance(raw_instance, EnergyBalance):
        return UIEnergyBalance(raw_instance)
    if isinstance(raw_instance, ClosedTank):
        return UIClosedTank(raw_instance)
    if isinstance(raw_instance, PressureBuildingDevice):
        return UIPressureBuildingDevice(raw_instance)
    return UINodeBase(raw_instance)
