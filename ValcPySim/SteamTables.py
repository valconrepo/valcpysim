from ctREFPROP.ctREFPROP import REFPROPFunctionLibrary
import os



class Tables:
    def __init__(self):
        path = os.environ ['RPPREFIX']
        self.Library = REFPROPFunctionLibrary(os.path.join(path, 'REFPROP.DLL'))
        self.Library.SETPATHdll(path)
        self.UNITS = self.Library.SI_WITH_C

    def get_parameter (self, output, **kwargs):
        keys = list(kwargs.keys())
        values = list(kwargs.values())
        number_of_values = output.count(';')
        result = self.Library.REFPROPdll(self.fluid, ''.join(keys), output, self.UNITS, 0, 1, values [0], values [1], self.composition)
        if result.ierr > 100:
            raise ValueError(result.herr)
        return result.Output [0:number_of_values + 1]



class SteamTables(Tables):

    fluid =  'WATER'
    composition = [1.0]

    def __init__(self):
        super().__init__()



class FlueGasTables(Tables):

    fluid = 'Nitrogen * Oxygen * Carbon dioxide * Water'

    def __init__(self, composition = [0.766, 0.028, 0.147, 0.059]):
        super().__init__()
        self.composition = composition

    @property
    def composition(self):
        return self._composition

    @composition.setter
    def composition(self, var):
        if sum(var) != 1:
            raise ValueError('Composition sum must be equal to 1')
        elif len(var)!=4:
            raise ValueError(f'Composition must an array of 4: {self.fluid}')
        else:
            self._composition = var

    def composition_by_fuel(self, fuel_compostion):
        #to be implemented
        raise NotImplementedError


class AirTables(Tables):

    fluid = 'AIR.MIX'
    composition=[1.0]

    def __init__(self):
        super.__init__()


class State(object):
    state_variables = {
        'p': 0.1,
        't': 25,
        'h': None
    }

    def __init__(self, tables = SteamTables):
        self.tables = tables()
        self.initState()

    @property
    def tables(self):
        return self._tables

    @tables.setter
    def tables(self, new_table):
        if isinstance(new_table, Tables):
            self._tables = new_table()
            self.initState()
        else:
            raise ValueError('It is not an tables instance')

    def initState(self):
        for var in self.state_variables.keys():
            if self.state_variables[var] is not None:
                setattr(self, var, self.state_variables[var])
            else:
                setattr(self, var, self.tables(var, T = state_variables['t'], P=state_variables['p']))

    def updateState(self, **input_states):
        keys = input_states.keys()
        for var in keys:
            setattr(self, var, input_states[var])
        vars = [item for item in self.state_variables.keys() if item not in keys]
        for var in vars:
            setattr(self, var, self.tables(var, **input_states))

    def get_parameters(self, parameters):
        return self.tables(parameters, T=self.t, P=self.p)


def CreateState(fluid):
    tables = GetAllTables()[fluid]
    return State(tables)

def GetAllTables():
    return {cls.__name__: cls  for cls in Tables.__subclasses__()}






















    



















