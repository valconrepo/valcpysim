from opcua import Client
import time


class OPCUA_client():
    def __init__(self, address="opc.tcp://172.168.1.47:4840/freeopcua/server/"):
        self.address = address
        self.client = Client(self.address)
        self._connected = False

    def connect(self):
        self.client.connect()
        self.client.load_type_definitions()
        self.root = self.client.get_root_node()
        self.objects = self.client.get_objects_node()
        uri = "http://examples.freeopcua.github.io"
        self.idx = self.client.get_namespace_index(uri)
        self._connected = True

    def disconnect(self):
        self.client.disconnect()
        self._connected = False

    @property
    def address(self):
        return self._address

    @address.setter
    def address(self, value):
        self._address = value
        self.client = Client(self.address)

    def get_var_names(self):
        if self.is_connected():
            self.objects.get_children()
            self.varlist = self.objects.get_children() [1:]
            self.var_names = [node.get_browse_name().to_string() [2:] for node in self.varlist]
            return self.var_names
        else:
            return -1

    def get_value(self, MyVariable):
        if self.is_connected():
            value = self.root.get_child(
                ["0:Objects", f"2:{MyVariable}".format(self.idx), '2:Value'.format(self.idx)]).get_value()
            return value
        else:
            return -1

    def set_value(self, MyVariable, value):
        if self.is_connected():
            self.root.get_child(
                ["0:Objects", f"2:{MyVariable}".format(self.idx), '2:Value'.format(self.idx)]).set_value(value)

    def is_connected(self):
        try:
            if self._connected and self.client.uaclient._uasocket._thread.isAlive():
                return True
            else:
                return False
        except:
            return False
