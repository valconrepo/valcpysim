## Copyright 2015-2019 Ilgar Lunin, Pedro Cabrera

## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at

##     http://www.apache.org/licenses/LICENSE-2.0

## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.


from PyFlow.UI import RESOURCES_DIR
from PyFlow.UI.Canvas.UINodeBase import UINodeBase
from PyFlow.UI.Canvas.UICommon import NodeActionButtonInfo


class UIEnergyBalance(UINodeBase):
    def __init__(self, raw_node):
        super(UIEnergyBalance, self).__init__(raw_node)
        actionAddOut = self._menu.addAction("Add Fluid Output")
        actionAddOut.setData(NodeActionButtonInfo(RESOURCES_DIR + "/pin.svg"))
        actionAddOut.setToolTip("Adds fluid output")
        actionAddOut.triggered.connect(self.onAddOutPin)

    def onPinWasKilled(self, uiPin):
       pass

    def postCreate(self, jsonTemplate=None):
        super(UIEnergyBalance, self).postCreate(jsonTemplate)
        for inPin in self.UIinputs.values():
            inPin.setDisplayName(f'{inPin.name}')
            inPin.OnPinDeleted.connect(self.onPinWasKilled)

    def onAddOutPin(self):
        rawPin = self._rawNode.createOutput()
        uiPin = self._createUIPinWrapper(rawPin)
        uiPin.OnPinDeleted.connect(self.onPinWasKilled)
        uiPin.setDisplayName(f"{rawPin.name}")
        return uiPin

