from PyFlow.Core import NodeBase
from PyFlow.Core.NodeBase import NodePinsSuggestionsHelper
from PyFlow.Core.Common import *
from PyFlow.Packages.ValcPySim.SteamTables import SteamTables


class FluidOut(NodeBase):
    def __init__(self, name):
        super(FluidOut, self).__init__(name)
        self.FluidIn = self.createInputPin('FluidIn', 'SimPin', None, self.compute)

        self.T = self.createInputPin('T', 'FloatPin', 293.15)
        self.F = self.createInputPin('F', 'FloatPin', 0)
        self.P = self.createInputPin('P', 'FloatPin', 0.1013)

        self.steam_tables = SteamTables()

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('FloatPin')
        helper.addOutputDataType('FloatPin')
        helper.addInputStruct(StructureType.Single)
        helper.addOutputStruct(StructureType.Single)
        return helper

    @staticmethod
    def category():
        return 'Simulator'

    @staticmethod
    def keywords():
        return []

    @staticmethod
    def description():
        return "Outer limit for the energy balance equations"

    def compute(self, *args, **kwargs):


        _data_out = {}
        if self.F.getData() != 0:
            _data_out['F'] = self.F.getData()

        if self.T.getData() != 0:
            _data_out['T'] = self.T.getData()

        if self.P.getData() != 0:
            _data_out ['P'] = self.P.getData()

        try:
            [_H] = self.steam_tables.get_parameter('H', P=self.P.getData(), T=self.T.getData())
            _data_out['H'] = _H
        except:
            pass

        self.FluidIn.setData(_data_out)