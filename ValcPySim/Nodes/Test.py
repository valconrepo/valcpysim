import numpy as np
from control.matlab import *
import matplotlib.pyplot as plt


_k = 0.000000001
_A0 = 10
_tq = 900
_tb = 900


F = 1
H = 50000

_Tdr =  25
_Pfur =  0.1014

Ta =  25
Af =  20

s = tf('s')


_c2 = 0.35

Q = [0]
Tb = [0]
m = [0]

for i in range(1000):
    _m = m[-1]
    A = np.array(
        [
            [0,                             _k * _m * H / (_tq * s + 1),                -_k * _m * _A0 / (_tq * s + 1)],
            [H * _c2 / (s * (_tb * s + 1)), _c2 * _k * _m * H / (s * (_tb * s + 1)),  _c2 * _k * _m * _A0 / (s * (_tb * s + 1))],
            [1 / s,                         -_k * _m * H / s,                           _k * _m * _A0 / s]

        ])



    B = np.array(
        [
            F,  Af  ,H
        ])

    u = [1, 1, 1, 1, 1, 1, 1, 1, 1 ,1]
    t = [0, .01, .02, .03, .04, .05, .06, .07, .08, .09]




    C = A @ B


    a = [lsim(m, u,t, x[-1]) for m, x in zip(C, [Q,Tb,m]) ]

    Q.append(a[0][0][-1])
    Tb.append(a[1][0][-1])
    m.append(max(a[2][0][-1], 0))

plt.plot(m)
plt.show()

