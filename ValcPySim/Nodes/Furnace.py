from PyFlow.Core import NodeBase
from PyFlow.Core.NodeBase import NodePinsSuggestionsHelper
from PyFlow.Core.Common import *
from PyFlow.Packages.ValcPySim.SteamTables import FlueGasTables
import numpy as np
from control.matlab import tf



class Furnace(NodeBase):
    def __init__(self, name):
        super(Furnace, self).__init__(name)

        self.PrimaryAir = self.createInputPin('Primary Air', 'SimPin', None, self.compute)

        self.FuelFlow =  self.createInputPin('Fuel Flow', 'FloatPin', 1)
        self.H = self.createInputPin('Fuel Specific heat', 'FloatPin', 50000)

        self.k = self.createInputPin('Parameter K', 'FloatPin', 1)
        self.A0 = self.createInputPin('Parameter A0', 'FloatPin', 10)
        self.tq = self.createInputPin('Bed to steam time constant', 'FloatPin', 10)
        self.tb = self.createInputPin('Gas to bed time constant', 'FloatPin', 10)

        self.ame = self.createInputPin('Heat transfer coeficient me-ev', 'FloatPin', 5000)
        self.ar = self.createInputPin('Heat transfer coeficient fg-me rad', 'FloatPin', 500)
        self.acr = self.createInputPin('Heat transfer coeficient fg-me con', 'FloatPin', 500)

        self.Ae = self.createInputPin('Heat transfer area', 'FloatPin', 5000)

        self.DrumHeat = self.createOutputPin('Drum Heat', 'SimPin')
        self.FlueGasOut = self.createOutputPin('Flue Gas Flow', 'SimPin')

        self._Tme = 25
        self._Tb = 25
        self._m = 0
        self._Q = 0


        self.FGtables = FlueGasTables()

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('FloatPin')
        helper.addOutputDataType('FloatPin')
        helper.addInputStruct(StructureType.Single)
        helper.addOutputStruct(StructureType.Single)
        return helper

    @staticmethod
    def category():
        return 'Simulator'

    @staticmethod
    def keywords():
        return []

    @staticmethod
    def description():
        return "Description in rst format."

    def compute(self, *args, **kwargs):

        _k = self.k.getData()
        _A0 = self.A0.getData()
        _tq = self.tq.getData()
        _tb = self.tb.getData()

        F = self.FuelFlow.getData()
        H = self.H.getData()


        _dataDrum = self.DrumHeat.getData() if self.DrumHeat.getData() is not None else {}
        _dataFur = self.FlueGasOut.getData() if self.FlueGasOut.getData() is not None else {}
        _dataAir = self.PrimaryAir.getData() if self.PrimaryAir.getData()is not None else {}

        _Tdr = _dataDrum['T'] if 'T' in _dataDrum else 25
        _Pfur = _dataFur['P'] if 'P' in _dataFur else 0.1014

        Ta = _dataAir['T'] if 'T' in _dataDrum else 25
        Af = _dataAir['F'] if 'F' in _dataFur else 0

        _ame = self.ame.getData()
        _Ae = self.Ae.getData()
        _Afn = _Ae
        _ar = self.ar.getData()
        _acr = self.acr.getData()

        s = tf('s')

        [_c2] = self.FGtables.get_parameter('CP', P=_Pfur, T=self._Tb)

        _m = m [-1]
        A = np.array(
            [
                [0, _k * self._m * H / (_tq * s + 1), -_k * self._m * _A0 / (_tq * s + 1)],
                [H * _c2 / (s * (_tb * s + 1)), _c2 * _k * self._m * H / (s * (_tb * s + 1)),
                 _c2 * _k * _m * _A0 / (s * (_tb * s + 1))],
                [1 / s, -_k * self._m * H / s, _k * self._m * _A0 / s]

            ])

        B = np.array(
            [
                F, Af, H
            ])

        u = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        t = [0, .1, .2, .3, .4, .5, .6, .7, .8, .9]

        C = A @ B

        a = [lsim(m, u, t, x0) for m, x0 in zip(C, [self._Q, self._Tb, self._m])]

        self._Q =       a [0] [0] [-1]
        self._Tb  =     a [1] [0] [-1]
        self._m = max(  a [2] [0] [-1], 0)

        Ffg = self._Q/H + Af

        def f(t, _Tme):
            self._qr = _ar * _Afn * ((self._Tb+273)**4 - (_Tme+273)**4) + _acr * _Afn * Ffg**0.8 * (self._Tb - _Tme)
            self._qme = _ame *_Ae * (_Tme -_Tdr) ** 3
            return (self._qr - self._qme)/(_mme * _cm)

        self._Tme = solve_ivp(f, [0, 1], [self._Tme])
        Qfg = self._Q - self._qr

        [Hpa] = self.FGtables.get_parameter('H', P=Pa, T=Ta)
        Tfg = self._Tb

        self.DrumHeat.setData(**_dataDrum, **{'Q': self._qme})
        self.DrumHeat.call()
        self.FlueGasOut.setData(**_dataFur, **{'F': Ffg, 'T': Tfg})
        self.FlueGasOut.call()
        self.PrimaryAir.setData(**_dataAir, **{'P': _Pfur})

































