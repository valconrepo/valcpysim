from PyFlow.Core import NodeBase
from PyFlow.Core.NodeBase import NodePinsSuggestionsHelper
from PyFlow.Core.Common import *
from PyFlow.Packages.ValcPySim.OPC import OPCUA_client


class OPC_Write(NodeBase):
    def __init__(self, name):
        super(OPC_Write, self).__init__(name)
        self.values = self.createInputPin('in', 'FloatPin', structure=StructureType.Dict, constraint="2")
        self.opc_client_node = self.createInputPin('opc', 'opcPin', None, self.compute)
        self.values.enableOptions(PinOptions.AllowMultipleConnections)



    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('ExecPin')
        helper.addInputDataType('FloatPin')
        helper.addInputStruct(StructureType.Single)
        helper.addOutputStruct(StructureType.Dict)
        return helper

    @staticmethod
    def category():
        return 'External Data'

    @staticmethod
    def keywords():
        return []

    @staticmethod
    def description():
        return "Description in rst format."

    def compute(self, *args, **kwargs):
        client = self.opc_client_node.getData()

        ySortedPins = sorted(self.values.affected_by, key=lambda pin: pin.owningNode().y)

        for i in ySortedPins:
            _data_dict = i.getData()
            for key in _data_dict:
                client.set_value(key, _data_dict[key])

