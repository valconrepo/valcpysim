from PyFlow import CreateRawPin
from PyFlow.Core import NodeBase
from PyFlow.Core.NodeBase import NodePinsSuggestionsHelper
from PyFlow.Core.Common import *
import scipy.linalg as linalg
import numpy as np
from PyFlow.Packages.ValcPySim.SteamTables import SteamTables


class EnergyBalance(NodeBase):
    def __init__(self, name):
        super(EnergyBalance, self).__init__(name)

        self.FluidIn = self.createInputPin('Fluid In', 'SimPin', None, self.compute)
        self.FluidOut = self.createOutputPin('FluidOut 1', 'SimPin')

        self.FluidIn.enableOptions(PinOptions.AllowMultipleConnections)

        self._pressure = 0.1012
        self._temperature = 25
        self.steam_tables = SteamTables()
        self.update_state()
        self.V = self.createInputPin('Volume', 'FloatPin', 10)

    def update_state(self):
        [self.h, rho_raw, self.dhdP, drhodP_raw, self.dhdP, drhodT_raw, self.dhdT] = \
            self.steam_tables.get_parameter('H;D;DHDP_T;DDDP;DHDP_T;DDDT;DHDT_P', P=self._pressure,
                                            T=self._temperature)
        self.rho = rho_raw
        self.drhodP = drhodP_raw
        self.drhodT = drhodT_raw

    def createOutput(self, *args, **kwargs):
        pinName= 'FluidOut ' + str((len(self.outputs)+1)).split('.')[0]
        p = CreateRawPin(pinName, self, 'SimPin', PinDirection.Output)
        p.enableOptions(PinOptions.Dynamic)
        p.enableOptions(PinOptions.AllowMultipleConnections)
        return p


    
    def serialize(self):
        data = super(EnergyBalance, self).serialize()
        data['numOutputs'] = len(self.outputs)
        return data

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('FloatPin')
        helper.addOutputDataType('FloatPin')
        helper.addInputStruct(StructureType.Single)
        helper.addOutputStruct(StructureType.Single)
        return helper

    @staticmethod
    def category():
        return 'Simulator'

    @staticmethod
    def keywords():
        return []

    @staticmethod
    def description():
        return "First energy balance for testing"

    def compute(self, *args, **kwargs):
        def f(t, r):

            self.update_state()
            _volume = self.V.getData()

            time_constants =np.matrix([
                [_volume* (self.rho*self.dhdP + self.h*self.drhodP - 1), _volume*(self.rho*self.dhdT + self.h*self.drhodT)],
                [_volume*self.drhodP,                                    _volume*self.drhodT]

            ])

            inputs = np.matrix([
                [_inlet_energy_flow - _outlet_energy_flow],
                [_inlet_flow - _outlet_flow]
            ])
            x = linalg.solve(time_constants, inputs)

            return x

        ySortedPinsIn = sorted(self.FluidIn.affected_by, key=lambda pinIn: pinIn.owningNode().y)

        
        

        _inlet_flow = 0.0
        _inlet_energy_flow = 0.0

        _outlet_flow = 0.0
        _outlet_energy_flow = 0.0

        _stateIn = []
        _stateOut = []

        for pinIn in ySortedPinsIn:

            _stateIn.append(pinIn.getData() if pinIn.getData()is not None else {})

            if 'F' in _stateIn[-1] and  'H' in _stateIn[-1]:
                _inlet_flow += _stateIn[-1]['F']
                _inlet_energy_flow +=(_stateIn[-1]['F']*_stateIn[-1]['H'])


        for pinOut in self.outputs.items():

            _stateOut.append (pinOut[1].getData() if pinOut[1].getData()is not None else {})

            if 'F' in _stateOut[-1]:
                _outlet_flow += _stateOut[-1]['F']
                _outlet_energy_flow += _stateOut[-1]['F']* self.h


        step = 100  # per second
        for i in range(step):

            #print([_inlet_flow, _inlet_energy_flow, _outlet_flow, _outlet_energy_flow ])

            #states = np.array([self._pressure, self._temperature])
            #_solution = solve_ivp(f, [0, 0.01], states)

            x = f(0,0)
            dP = x[0][0]
            dT = x[1][0]


            self._pressure +=dP/step
            self._temperature += dT/step

        self._temperature = max(self._temperature, 25)
        self._temperature = min(self._temperature, 600)
        self._pressure = max(self._pressure, 0.01)
        self._pressure = min(self._pressure, 30)

        #print([self._pressure, self._temperature])
        
        _state_update_Out = {'H': self.h, 'T': self._temperature, 'P': self._pressure}

        for i, pinOut in enumerate(self.outputs.items()):
            pinOut[1].setData({**_stateOut[i], **_state_update_Out})
            pinOut[1].call()

        _state_update_In = {'P': self._pressure}

        
        for i, pinIn in  enumerate(ySortedPinsIn):
            pinIn.setData({**_stateIn[i], **_state_update_In})






