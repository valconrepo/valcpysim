from PyFlow.Core import NodeBase
from PyFlow.Core.NodeBase import NodePinsSuggestionsHelper
from PyFlow.Core.Common import *
from PyFlow.Packages.ValcPySim.SteamTables import SteamTables


class FluidIn(NodeBase):
    def __init__(self, name):
        super(FluidIn, self).__init__(name)
        self.inExec = self.createInputPin('inExec', 'ExecPin', None, self.compute)

        self.T = self.createInputPin('T', 'FloatPin', 293.15)
        self.F = self.createInputPin('F', 'FloatPin', 0)
        self.P = self.createInputPin('P', 'FloatPin', 0.1013)

        self.FluidOut = self.createOutputPin('FluidOut', 'SimPin')
        self.steam_tables = SteamTables()

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('FloatPin')
        helper.addOutputDataType('FloatPin')
        helper.addInputStruct(StructureType.Single)
        helper.addOutputStruct(StructureType.Single)
        return helper

    @staticmethod
    def category():
        return 'Simulator'

    @staticmethod
    def keywords():
        return []

    @staticmethod
    def description():
        return "Inlet limit for the energy balance eqations"

    def compute(self, *args, **kwargs):
        [_H] = self.steam_tables.get_parameter('H', P=self.P.getData(), T=self.T.getData())

        _data_out = {'F': self.F.getData(), 'T': self.T.getData(),
                     'P': self.P.getData(), 'H': _H}
        self.FluidOut.setData(_data_out)
        self.FluidOut.call()
