## Copyright 2015-2019 Ilgar Lunin, Pedro Cabrera

## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at

##     http://www.apache.org/licenses/LICENSE-2.0

## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.


from PyFlow.Core import NodeBase
from PyFlow.Core.NodeBase import NodePinsSuggestionsHelper
from PyFlow.Core.Common import *
from PyFlow.Core.structs import splineRamp
from PyFlow.Packages.ValcPySim.SteamTables import *
from scipy.integrate import solve_ivp


class PressureBuildingDevice(NodeBase):
    def __init__(self, name):
        super(PressureBuildingDevice, self).__init__(name)
        self.bCacheEnabled = False



        self.FluidIn = self.createInputPin('FluidIn', 'SimPin', None, self.compute)
        self.FluidOut = self.createOutputPin('FluidOut', 'SimPin')



        self.V = self.createInputPin('Volume', 'FloatPin', 1)
        self.ref_P = self.createInputPin('Reference Pressure', 'FloatPin', 10)
        self.ref_F = self.createInputPin('Reference Flow', 'FloatPin', 10)

        self.ramp = splineRamp()
        self._curveTypes = ["linear", "bezier"]
        self._curveType = 0

        self._fluidTypes = ['Water', 'Air']
        self._fluidType = 0

        self.tables = SteamTables() if self._fluidTypes[self._fluidType]=='Water' else AirTables()

        self.solve(0.1)

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('FloatPin')
        helper.addOutputDataType('FloatPin')
        helper.addInputStruct(StructureType.Single)
        helper.addOutputStruct(StructureType.Single)

        return helper

    def serialize(self):
        orig = super(PressureBuildingDevice, self).serialize()
        orig["ramp"] = [[x.getU(),x.getV()] for x in self.ramp.sortedItems()]
        orig["curveType"] = self._curveType
        return orig

    def postCreate(self, jsonTemplate=None):
        super(PressureBuildingDevice, self).postCreate(jsonTemplate)
        if "ramp" in jsonTemplate:
            for x in jsonTemplate["ramp"]:
                self.ramp.addItem(x[0],x[1])
        if "curveType" in jsonTemplate:
                self._curveType = jsonTemplate["curveType"]

    @staticmethod
    def category():
        return 'Simulator'

    @staticmethod
    def keywords():
        return []

    @staticmethod
    def description():
        return 'Pressure build up device'

    def compute(self, *args, **kwargs):

        self.solve(self._p.y[-1])

        [_H, _T] = self.tables.get_parameter('H;T',  P=self._p.y, S=self._s)

        _updateDataIn = {'F': self._in_flow}
        _updateDataOut = {'P': self._p.y + self._pressIn, 'T': _T, 'H': _H}

        self.FluidIn.setData({**self._inData, **_updateDataIn})
        self.FluidOut.setData({**self._outData, **_updateDataOut})


    def solve(self, y0):
        def f( t, y):
            return (self._in_flow - _out_flow) / (self.V.getData() * _betas * _rho)

        self._inData = self.FluidIn.getData() if self.FluidIn.getData() is not None else {}
        self._outData = self.FluidOut.getData() if self.FluidOut.getData() is not None else {}
        _out_flow = self._outData ['F'] if 'F' in self._outData else 0

        self._tempIn = self._inData['T'] if 'T' in self._inData else 25
        self._pressIn = self._inData['P'] if 'P' in self._inData else 0.1014


        bezier = self._curveTypes [self._curveType] == "bezier"
        self._in_flow = self.ramp.evaluateAt(y0/self.ref_P.getData(), bezier)*self.ref_F.getData()

        [_rho, _betas, self._s] = self.tables.get_parameter("D;BETAS;S", T=self._tempIn, P=self._pressIn)

        self._p = solve_ivp(f, [0, 1], [y0])
