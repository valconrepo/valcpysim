from PyFlow.Core import NodeBase
from PyFlow.Core.NodeBase import NodePinsSuggestionsHelper
from PyFlow.Core.Common import *
from PyFlow.Packages.ValcPySim.OPC import OPCUA_client


class OPC_Read(NodeBase):
    def __init__(self, name):
        super(OPC_Read, self).__init__(name)
        self.inExec = self.createInputPin('inExec', 'ExecPin', None, self.compute)
        self.address = self.createInputPin('address', 'StringPin')
        self.values = self.createOutputPin('out', 'FloatPin', structure=StructureType.Dict, constraint="2")
        self.opc_node = self.createOutputPin('opc', 'opcPin')
        self.client = OPCUA_client()

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('ExecPin')
        helper.addInputDataType('StringPin')
        helper.addOutputDataType('AnyPin')
        helper.addInputStruct(StructureType.Single)
        helper.addOutputStruct(StructureType.Dict)
        return helper

    @staticmethod
    def category():
        return 'External Data'

    @staticmethod
    def keywords():
        return []

    @staticmethod
    def description():
        return "Description in rst format."

    def compute(self, *args, **kwargs):
        if not self.client.is_connected():
            try:
                self.client.address = self.address.getData()
                self.client.connect()
                print('Connected!')
            except:
                self.client.disconnect()
                print('Cannot connect!')
        _data_dict = PFDict('StringPin')
        for variable in self.client.get_var_names():
            _data_dict[variable] = self.client.get_value(variable) if self.client.get_value(variable) is not None else 0
        self.values.setData(_data_dict)
        self.values.setDirty()
        self.opc_node.setData(self.client)
        self.opc_node.call()
        