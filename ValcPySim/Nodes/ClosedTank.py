from PyFlow import CreateRawPin
from PyFlow.Core import NodeBase
from PyFlow.Core.NodeBase import NodePinsSuggestionsHelper
from PyFlow.Core.Common import *
import scipy.linalg as linalg
import numpy as np
from PyFlow.Packages.ValcPySim.SteamTables import SteamTables


class ClosedTank(NodeBase):
    cp = 20
    def __init__(self, name):
        super(ClosedTank, self).__init__(name)

        self.V = self.createInputPin('TotalVolume', 'FloatPin', 10)
        self.m = self.createInputPin('mass', 'FloatPin', 1000)
        self.steam_out = self.createInputPin('steam out', 'BoolPin', True)

        self.FluidIn = self.createInputPin('Fluid In', 'SimPin', None, self.compute)
        self.FluidIn.enableOptions(PinOptions.AllowMultipleConnections)

        self.FluidOut = self.createOutputPin('Fluid Out', 'SimPin')
        self.DrumHeat = self.createInputPin('Drum Heat', 'SimPin')


        self.level = self.createOutputPin('Level', 'FloatPin')

        self.T = 25
        self.V_w = 0
        self.steam_tables = SteamTables()
        self.update_state()



    def update_state(self):
        #water parameters
        [self.P, self.rho_w, self.drhodT_w, self.h_w, self.dhdT_w] = \
            self.steam_tables.get_parameter('P;D;DDDT;H;DHDT_P', Q=0, T=self.T )

        #steam parameters
        [self.rho_s, self.drhodT_s, self.h_s, self.dhdT_s] = \
            self.steam_tables.get_parameter('D;DDDT;H;DHDT_P', Q=1, T=self.T )

        #allong saturation line
        [self.dPdT] = \
            self.steam_tables.get_parameter('DPDTSAT', Q=1, T=self.T )




    def createInput(self, *args, **kwargs):
        pinName = 'Flow ' + str((len(self.inputs) + 1)).split('.') [0]
        p = CreateRawPin(pinName, self, 'SimPin', PinDirection.Output)
        p.enableOptions(PinOptions.Dynamic)
        return p


    def serialize(self):
        data = super(ClosedTank, self).serialize()
        data ['numInputs'] = len(self.inputs)
        return data

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('FloatPin')
        helper.addOutputDataType('FloatPin')
        helper.addInputStruct(StructureType.Single)
        helper.addOutputStruct(StructureType.Single)
        return helper

    @staticmethod
    def category():
        return 'Simulator'

    @staticmethod
    def keywords():
        return []

    @staticmethod
    def description():
        return "Closed tankenergy balance solution"

    def compute(self, *args, **kwargs):
        def f(t, r):

            self.V_s = self.V.getData() - self.V_w

            self.update_state()


            e11 = self.rho_w - self.rho_s
            e12 = self.V_s * self.drhodT_s + self.V_w * self.drhodT_w
            e21 = self.rho_w * self.h_w - self.rho_s * self.h_s
            e22 = self.V_s * (self.h_s * self.drhodT_s) + self.V_w * (
                        self.h_w * self.drhodT_w + self.rho_w * self.dhdT_w) - \
                  self.V.getData() * self.dPdT + self.m.getData() * self.cp

            time_constants = np.matrix([
                [e11, e12],
                [e21, e22]

            ])

            inputs = np.matrix([
                [_inlet_flow - _outlet_flow],
                [_inlet_energy_flow - _outlet_energy_flow + _heat_load],
            ])

            x = linalg.solve(time_constants, inputs)

            return x

        ySortedPinsIn = sorted(self.FluidIn.affected_by, key=lambda pinIn: pinIn.owningNode().y)

        _inlet_flow = 0.0
        _inlet_energy_flow = 0.0

        _outlet_flow = 0.0
        _outlet_energy_flow = 0.0

        _stateIn = []
        _stateOut = []

        h = self.h_s if self.steam_out.getData() == True else self.h_w
        _dataDrum = self.DrumHeat.getData() if self.DrumHeat.getData() is not None else {}
        _heat_load = _dataDrum['Q'] if 'Q' in _dataDrum else 0


        for pinIn in ySortedPinsIn:

            _stateIn.append(pinIn.getData() if pinIn.getData()is not None else {})

            if 'F' in _stateIn[-1] and  'H' in _stateIn[-1]:
                _inlet_flow += _stateIn[-1]['F']
                _inlet_energy_flow +=(_stateIn[-1]['F']*_stateIn[-1]['H'])

        for pinOut in self.outputs.items():

            _stateOut.append(pinOut [1].getData() if pinOut [1].getData() is not None else {})

            if  type(_stateOut[-1]) is not float:
                if 'F' in _stateOut [-1]:
                    _outlet_flow += _stateOut [-1] ['F']
                    _outlet_energy_flow += _stateOut [-1] ['F'] * h


        step = 100  # per second
        for i in range(step):


            x = f(0, 0)
            dV = x [0][0]
            dT = x [1][0]
            self.V_w += dV / step
            self.T += dT / step
            
        self.T = max(self.T, 25)
        self.T = min(self.T, 600)
        self.V_w = max(self.V_w, 0.01)
        self.V_w = min(self.V_w, 300)

        _state_update_Out = {'H': h, 'T': self.T, 'P': self.P}

        for i, pinOut in enumerate(self.outputs.items()):
            if type(_stateOut [i]) is not float:
                pinOut [1].setData({**_stateOut [i], **_state_update_Out})
                pinOut [1].call()

        _state_update_In = {'P': self.P}

        for i, pinIn in enumerate(ySortedPinsIn):
            pinIn.setData({**_stateIn [i], **_state_update_In})

        self.DrumHeat.set({**_dataDrum, ** _state_update_Out})


