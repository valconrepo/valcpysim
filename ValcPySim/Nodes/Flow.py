from PyFlow.Core import NodeBase
from PyFlow.Core.NodeBase import NodePinsSuggestionsHelper
from PyFlow.Core.Common import *
from math import sqrt



class Flow(NodeBase):
    def __init__(self, name):
        super(Flow, self).__init__(name)

        self.FluidIn = self.createInputPin('FluidIn', 'SimPin', None, self.compute)
        self.FluidOut = self.createOutputPin('FluidOut', 'SimPin')

        self.FluidIn.enableOptions(PinOptions.AllowMultipleConnections)
        self.FluidOut.enableOptions(PinOptions.AllowMultipleConnections)

        self.gain = self.createInputPin('Gain', 'FloatPin', 1)


    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('FloatPin')
        helper.addOutputDataType('FloatPin')
        helper.addInputStruct(StructureType.Single)
        helper.addOutputStruct(StructureType.Single)
        return helper

    @staticmethod
    def category():
        return 'Simulator'

    @staticmethod
    def keywords():
        return []

    @staticmethod
    def description():
        return "Simulates pipe"

    def compute(self, *args, **kwargs):

        self._data_in = self.FluidIn.getData()
        self._data_out = self.FluidOut.getData() if self.FluidOut.getData() is not None else {}

        if 'P' in self._data_out:
            _dP = self._data_in['P'] - self._data_out['P']
        else:
            _dP = 0


            
        _direction = 1 if _dP > 0 else -1
        self._flow = sqrt(abs(_dP))*_direction*self.gain.getData()

        _data_update_Out = {'F': self._flow, 'H':self._data_in['H'], 'T': self._data_in['T']}

        self.FluidOut.setData({**self._data_out, **_data_update_Out})
        self.FluidOut.call()
        
        _data_update_In = {'F': self._flow}

        self.FluidIn.setData({**self._data_in, **_data_update_In})

