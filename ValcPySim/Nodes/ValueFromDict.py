from PyFlow.Core import NodeBase
from PyFlow.Core.NodeBase import NodePinsSuggestionsHelper
from PyFlow.Core.Common import *


class ValueFromDict(NodeBase):
    def __init__(self, name):
        super(ValueFromDict, self).__init__(name)
        self.values = self.createInputPin('inp', 'FloatPin', structure=StructureType.Dict, constraint="2")
        self.point_name =  self.createInputPin('dictKey', 'StringPin')
        self.out = self.createOutputPin('out', 'FloatPin',)

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('FloatPin')
        helper.addInputDataType('StringPin')
        helper.addOutputDataType('FloatPin')
        helper.addInputStruct(StructureType.Dict)
        helper.addOutputStruct(StructureType.Single)
        return helper

    @staticmethod
    def category():
        return 'Misc'

    @staticmethod
    def keywords():
        return []

    @staticmethod
    def description():
        return "Gets value from dictionary"

    def compute(self, *args, **kwargs):
        point_name_str = self.point_name.getData()
        values_dict = self.values.getData()
        self.out.setData(values_dict[point_name_str])