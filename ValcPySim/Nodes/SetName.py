from PyFlow.Core import NodeBase
from PyFlow.Core.NodeBase import NodePinsSuggestionsHelper
from PyFlow.Core.Common import *


class SetName(NodeBase):
    def __init__(self, name):
        super(SetName, self).__init__(name)
        self.value = self.createInputPin('inp', 'FloatPin')
        self.dict = self.createOutputPin('out', 'FloatPin', structure=StructureType.Dict, constraint="2")
        self.dictKey = self.createInputPin('dictKey', 'StringPin')

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('FloatPin')
        helper.addInputDataType('StringPin')
        helper.addOutputDataType('FloatPin')
        helper.addInputStruct(StructureType.Single)
        helper.addOutputStruct(StructureType.Dict)
        return helper

    @staticmethod
    def category():
        return 'Misc'

    @staticmethod
    def keywords():
        return []

    @staticmethod
    def description():
        return "Sets name for the output"

    def compute(self, *args, **kwargs):
        _dict_var = PFDict('StringPin')
        _dict_var[self.dictKey.getData()] = self.value.getData()
        self.dict.setData(_dict_var)