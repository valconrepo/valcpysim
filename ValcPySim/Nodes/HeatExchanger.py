from PyFlow.Core import NodeBase
from PyFlow.Core.NodeBase import NodePinsSuggestionsHelper
from PyFlow.Core.Common import *
from PyFlow.Packages.ValcPySim.SteamTables import *
from scipy.integrate import solve_ivp


class HeatExchanger(NodeBase):

    slices = 10 #number of slices
    cs = 502

    def __init__(self, name):
        super(HeatExchanger, self).__init__(name)


        self.Fluid1In =  self.createInputPin('Fluid 1 In', 'SimPin', None, self.compute)
        self.Fluid2In = self.createInputPin('Fluid 2 In', 'SimPin', None, self.compute)

        
        self.Fluid1Out = self.createOutputPin('Fluid 1 Out', 'SimPin')
        self.Fluid2Out = self.createOutputPin('Fluid 2 Out', 'SimPin')



        self.L =  self.createInputPin('Length', 'FloatPin', 1000)
        self.G =  self.createInputPin('Mass', 'FloatPin', 1000)
        self.inner_diam= self.createInputPin('InnerDiameter', 'FloatPin', 0.01)
        self.outer_diam = self.createInputPin('OuterDiameter', 'FloatPin', 0.01)
        self.cross_sec_1 = self.createInputPin('Crossesction 1', 'FloatPin', 1)
        self.cross_sec_2 = self.createInputPin('Crossesction 2', 'FloatPin', 1)
        self.alfa_1 = self.createInputPin('HeatTransfer 1', 'FloatPin', 20)
        self.alfa_2 = self.createInputPin('HeatTransfer 2', 'FloatPin', 2000)



        self.steamtables =  SteamTables()
        self.fluegastables = FlueGasTables()

        initial_temp = 25

        self.Ts = [initial_temp for i in range(self.slices)]
        self.T1 = [initial_temp for i in range(self.slices)]
        self.T2 = [initial_temp for i in range(self.slices)]


    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('FloatPin')
        helper.addOutputDataType('FloatPin')
        helper.addInputStruct(StructureType.Single)
        helper.addOutputStruct(StructureType.Single)
        return helper

    @staticmethod
    def category():
        return 'Simulator'

    @staticmethod
    def keywords():
        return []

    @staticmethod
    def description():
        return "Description in rst format."

    def compute(self, *args, **kwargs):
        def dT1dt_1(t, y):
            return 1/tau1*(self.Ts[0]-y)-u1/(2*h)*(self.T1[1]-T1in)
        def dT2dt_1(t, y):
            return 1/tau2*(self.Ts[0]-y)-u2/(2*h)*(self.T2[1]-T2in)
        def dTsdt_1(t, y):
            return 1/taus1*(self.T1[0]-y)+1/taus2*(self.T2[0]-y)
        def dT1dt_i(t, y):
            return 1/tau1*(self.Ts[i]-y)-u1/(2*h)*(self.T1[i+1]-self.T1[i-1])
        def dT2dt_i(t, y):
            return 1/tau2*(self.Ts[i]-y)-u2/(2*h)*(self.T2[i+1]-self.T2[i- 1])
        def dTsdt_i(t, y):
            return 1/taus1*(self.T1[i]-y)+1/taus2*(self.T2[i]-y)
        def dT1dt_N(t, y):
            return 1/tau1*(self.Ts[N-1]-y)-u1/(2*h)*(self.T1[N-3]-4*self.T1[N-2]+3*y)
        def dT2dt_N(t, y):
            return 1/tau2*(self.Ts[N-1]-y)-u2/(2*h)*(self.T2[N-3]-4*self.T2[N-2]+3*y)
        def dTsdt_N(t, y):
            return 1/taus1*(self.T1[N-1]-y)+1/taus2*(self.T2[N-1]-y);

        N = self.slices

        h = self.L.getData()/N

        iterations= 1000

        _F1DataIn = self.Fluid1In.getData() if self.Fluid1In.getData() is not None else {}
        _F2DataIn = self.Fluid2In.getData() if self.Fluid2In.getData() is not None else {}

        _F1DataOut = self.Fluid1Out.getData() if self.Fluid1Out.getData() is not None else {}
        _F2DataOut = self.Fluid2Out.getData() if self.Fluid2Out.getData() is not None else {}


        T1in = _F1DataIn['T'] if 'T' in  _F1DataIn else 25
        T2in = _F2DataIn['T'] if 'T' in  _F2DataIn else 25

        self.P1in = _F1DataOut['P'] if 'P' in  _F1DataOut else 0.10014
        self.P2in = _F2DataOut['P'] if 'P' in  _F2DataOut else 0.10014

        Flow1in = _F1DataIn['F'] if 'F' in  _F1DataIn else 0
        Flow2in = _F2DataIn ['F'] if 'F' in _F2DataIn else 0

        [c1, ro1] = self.steamtables.get_parameter('CP;D',  T=T1in, P=self.P1in)
        [c2, ro2] = self.fluegastables.get_parameter('CP;D',  T=T2in, P=self.P2in)

        A1 = self.inner_diam.getData()
        A2 = self.outer_diam.getData()
        F1 = self.cross_sec_1.getData()
        F2 = self.cross_sec_2.getData()
        as1 = self.alfa_1.getData()
        as2 = self.alfa_2.getData()
        cs = self.cs
        G = self.G.getData()

        u1 = Flow1in/(ro1*F1)
        u2 = Flow2in/(ro2*F2)

        O1 = A1/self.L.getData()
        O2 = A2/self.L.getData()

        tau1=c1*1000*ro1*F1/(as1*O1)
        tau2=c2*1000*ro2*F2/(as2*O2)
        taus1=cs*G/(as1*O1)
        taus2=cs*G/(as2*O2)

        self.T1[0] = solve_ivp(dT1dt_1, [0, 1], [self.T1[0]]).y[0][-1]
        self.T2[0] = solve_ivp(dT2dt_1, [0, 1], [self.T2[0]]).y[0][-1]
        self.Ts[0] = solve_ivp(dT2dt_1, [0, 1], [self.Ts[0]]).y[0][-1]

        for i in range(1, N-1, 1):
            self.T1[i] = solve_ivp(dT1dt_i, [0, 1], [self.T1[i]]).y[0][-1]
            self.T2[i] = solve_ivp(dT2dt_i, [0, 1], [self.T2[i]]).y[0][-1]
            self.Ts[i] = solve_ivp(dTsdt_i, [0, 1], [self.Ts[i]]).y[0][-1]

        self.T1 [N-1] = solve_ivp(dT1dt_N, [0, 1], [self.T1 [N-1]]).y[0][-1]
        self.T2 [N-1] = solve_ivp(dT2dt_N, [0, 1], [self.T2 [N-1]]).y[0][-1]
        self.Ts [N-1] = solve_ivp(dTsdt_N, [0, 1], [self.Ts [N-1]]).y[0][-1]



        [self._H1] = self.steamtables.get_parameter('H',  T=self.T1[-1], P=self.P1in)
        [self._H2] = self.fluegastables.get_parameter('H', T=self.T2 [-1], P=self.P2in)


        _stateUpdateOut1 = { 'T': self.T1[-1], 'H': self._H1, 'F': Flow1in}
        _stateUpdateOut2 = { 'T': self.T2[-1], 'H': self._H2, 'F': Flow2in}

        self.Fluid1Out.setData({**_F1DataOut, **_stateUpdateOut1})
        self.Fluid1Out.call()
        self.Fluid2Out.setData({**_F2DataOut, **_stateUpdateOut2})
        self.Fluid2Out.call()

        _stateUpdateIn1 = {'P':self.P1in}
        _stateUpdateIn2 = {'P':self.P2in}

        self.Fluid1In.setData({**_F1DataIn, **_stateUpdateIn1})
        self.Fluid2In.setData({**_F2DataIn, **_stateUpdateIn2})









