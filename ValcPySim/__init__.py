PACKAGE_NAME = 'ValcPySim'

from collections import OrderedDict
from PyFlow.UI.UIInterfaces import IPackage

# Pins
from PyFlow.Packages.ValcPySim.Pins.DemoPin import DemoPin
from PyFlow.Packages.ValcPySim.Pins.opcPin import opcPin
from PyFlow.Packages.ValcPySim.Pins.SimPin import SimPin

# Function based nodes
#from PyFlow.Packages.ValcPySim.FunctionLibraries.DemoLib import DemoLib

# Class based nodes
#from PyFlow.Packages.ValcPySim.Nodes.DemoNode import DemoNode
from PyFlow.Packages.ValcPySim.Nodes.EnergyBalance import EnergyBalance
from PyFlow.Packages.ValcPySim.Nodes.OPC_Read import OPC_Read
from PyFlow.Packages.ValcPySim.Nodes.OPC_Write import OPC_Write
from PyFlow.Packages.ValcPySim.Nodes.ValueFromDict import ValueFromDict
from PyFlow.Packages.ValcPySim.Nodes.SetName import SetName
from PyFlow.Packages.ValcPySim.Nodes.Flow import Flow
from PyFlow.Packages.ValcPySim.Nodes.ClosedTank import ClosedTank
from PyFlow.Packages.ValcPySim.Nodes.HeatExchanger import HeatExchanger
from PyFlow.Packages.ValcPySim.Nodes.PressureBuildingDevice import PressureBuildingDevice
from PyFlow.Packages.ValcPySim.Nodes.FluidIn import FluidIn
from PyFlow.Packages.ValcPySim.Nodes.FluidOut import FluidOut
from PyFlow.Packages.ValcPySim.Nodes.Furnace import Furnace



# Tools
#from PyFlow.Packages.ValcPySim.Tools.DemoShelfTool import DemoShelfTool
#from PyFlow.Packages.ValcPySim.Tools.DemoDockTool import DemoDockTool

# Exporters
#from PyFlow.Packages.ValcPySim.Exporters.DemoExporter import DemoExporter

# Factories
from PyFlow.Packages.ValcPySim.Factories.UIPinFactory import createUIPin
from PyFlow.Packages.ValcPySim.Factories.UINodeFactory import createUINode
from PyFlow.Packages.ValcPySim.Factories.PinInputWidgetFactory import getInputWidget

# Prefs widgets
#from PyFlow.Packages.ValcPySim.PrefsWidgets.DemoPrefs import DemoPrefs

_FOO_LIBS = {}
_NODES = {}
_PINS = {}
_TOOLS = OrderedDict()
_PREFS_WIDGETS = OrderedDict()
_EXPORTERS = OrderedDict()

#_FOO_LIBS[DemoLib.__name__] = DemoLib(PACKAGE_NAME)

#_NODES[DemoNode.__name__] = DemoNode
_NODES[EnergyBalance.__name__] = EnergyBalance
_NODES[OPC_Read.__name__] = OPC_Read
_NODES[OPC_Write.__name__] = OPC_Write
_NODES[ValueFromDict.__name__] = ValueFromDict
_NODES[SetName.__name__] = SetName
_NODES[Flow.__name__] = Flow
_NODES[ClosedTank.__name__] = ClosedTank
_NODES[HeatExchanger.__name__] = HeatExchanger
_NODES[PressureBuildingDevice.__name__] = PressureBuildingDevice
_NODES[FluidIn.__name__] = FluidIn
_NODES[FluidOut.__name__] = FluidOut
_NODES[Furnace.__name__] = Furnace


_PINS[DemoPin.__name__] = DemoPin
_PINS[opcPin.__name__] = opcPin
_PINS[SimPin.__name__] = SimPin


#_TOOLS[DemoShelfTool.__name__] = DemoShelfTool
#_TOOLS[DemoDockTool.__name__] = DemoDockTool

#_EXPORTERS[DemoExporter.__name__] = DemoExporter

#_PREFS_WIDGETS["Demo"] = DemoPrefs


class ValcPySim(IPackage):
	def __init__(self):
		super(ValcPySim, self).__init__()

	@staticmethod
	def GetExporters():
		return _EXPORTERS

	@staticmethod
	def GetFunctionLibraries():
		return _FOO_LIBS

	@staticmethod
	def GetNodeClasses():
		return _NODES

	@staticmethod
	def GetPinClasses():
		return _PINS

	@staticmethod
	def GetToolClasses():
		return _TOOLS

	@staticmethod
	def UIPinsFactory():
		return createUIPin

	@staticmethod
	def UINodesFactory():
		return createUINode

	@staticmethod
	def PinsInputWidgetFactory():
		return getInputWidget

	@staticmethod
	def PrefsWidgets():
		return _PREFS_WIDGETS

