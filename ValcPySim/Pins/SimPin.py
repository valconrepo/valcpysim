from PyFlow.Core import PinBase
from PyFlow.Core.Common import *
import json
from PyFlow.Packages.ValcPySim.OPC import OPCUA_client



class NoneEncorder(json.JSONEncoder):
    def default(self, opc):
        return None

class NoneDecoder(json.JSONDecoder):
    def __init__(self, *args, **kwargs):
        super(NoneDecoder, self).__init__(object_hook = self.object_hook, *args, **kwargs)

    def object_hook(self, opc_dic):
        return None






class SimPin(PinBase):
    """doc string for DemoPin"""
    def __init__(self, name, parent, direction, **kwargs):
        super(SimPin, self).__init__(name, parent, direction, **kwargs)
        self.setDefaultValue({'F': 0, 'P':0, 'H': 0, 'T':0 })
        self.dirty = False
        self._isArray = False
        self._lastCallTime = 0.0

    @staticmethod
    def IsValuePin():
        return False

    @staticmethod
    def supportedDataTypes():
        return ('SimPin',)

    @staticmethod
    def pinDataTypeHint():
        return 'SimPin', None

    @staticmethod
    def color():
        return (72,61,139, 255)

    @staticmethod
    def internalDataStructure():
        return dict

    @staticmethod
    def processData(data):
        return data

    @staticmethod
    def jsonEncoderClass():
        return NoneEncorder

    @staticmethod
    def jsonDecoderClass():
        return NoneDecoder

    def getLastExecutionTime(self):
        return self._lastCallTime

    def call(self, *args, **kwargs):
        if self.owningNode().isValid():
            self._lastCallTime = currentProcessorTime()
        super(SimPin, self).call(*args, **kwargs)

    def isExec(self):
        return True

    def setData(self, data):
        """Sets value to pin

        :param data: Data to be set
        :type data: object
        """
        if self.super is None:
            return
        try:
            self.setClean()
            if isinstance(data, DictElement) and not self.optionEnabled(PinOptions.DictElementSupported):
                data = data[1]
            if not self.isArray() and not self.isDict():
                if isinstance(data, DictElement):
                    self._data = DictElement(data[0], self.super.processData(data[1]))
                else:
                    self._data = self.super.processData(data)
            elif self.isArray():
                if isinstance(data, list):
                    if self.validateArray(data, self.super.processData):
                        self._data = data
                    else:
                        raise Exception("Some Array Input is not valid Data")
                else:
                    self._data = [self.super.processData(data)]
            elif self.isDict():
                if isinstance(data, PFDict):
                    self._data = PFDict(data.keyType, data.valueType)
                    for key, value in data.items():
                        self._data[key] = self.super.processData(value)
                elif isinstance(data, DictElement) and len(data) == 2:
                    self._data.clear()
                    self._data[data[0]] = self.super.processData(data[1])
            if self.direction == PinDirection.Output:
                for i in self.affects:
                    i.setData(self.currentData())
                    i.setClean()
            if self.direction == PinDirection.Input:
                for i in self.affected_by:
                    i._data = self.currentData()
                    i.setClean()
            self.clearError()
            self.dataBeenSet.send(self)
        except Exception as exc:
            self.setError(exc)
            self.setDirty()
        if self._lastError is not None:
            self.owningNode().setError(self._lastError)
            wrapper = self.owningNode().getWrapper()
            if wrapper:
                wrapper.update()


            
