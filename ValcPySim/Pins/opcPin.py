from PyFlow.Core import PinBase
from PyFlow.Core.Common import *
import json
from PyFlow.Packages.ValcPySim.OPC import OPCUA_client

class NoneEncorder(json.JSONEncoder):
    def default(self, opc):
        return None

class NoneDecoder(json.JSONDecoder):
    def __init__(self, *args, **kwargs):
        super(NoneDecoder, self).__init__(object_hook = self.object_hook, *args, **kwargs)

    def object_hook(self, opc_dic):
        return None



class opcPin(PinBase):
    """doc string for DemoPin"""
    def __init__(self, name, parent, direction, **kwargs):
        super(opcPin, self).__init__(name, parent, direction, **kwargs)
        self.setDefaultValue(None)
        self.disableOptions(PinOptions.Storable)
        self.dirty = False
        self._isArray = False
        self._lastCallTime = 0.0

    @staticmethod
    def IsValuePin():
        return False

    @staticmethod
    def supportedDataTypes():
        return ('opcPin',)

    @staticmethod
    def pinDataTypeHint():
        return 'opcPin', None

    @staticmethod
    def color():
        return (200, 200, 50, 255)

    @staticmethod
    def internalDataStructure():
        return object

    @staticmethod
    def processData(data):
        return data

    @staticmethod
    def jsonEncoderClass():
        return NoneEncorder

    @staticmethod
    def jsonDecoderClass():
        return NoneDecoder

    def getLastExecutionTime(self):
        return self._lastCallTime

    def call(self, *args, **kwargs):
        if self.owningNode().isValid():
            self._lastCallTime = currentProcessorTime()
        super(opcPin, self).call(*args, **kwargs)

    def isExec(self):
        return True